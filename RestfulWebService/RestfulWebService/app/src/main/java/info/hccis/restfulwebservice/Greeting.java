package info.hccis.restfulwebservice;

/**
 * Created by Michael on 2015-01-29.
 */
public class Greeting {

    private String id;
    private String content;

    public String getId() {
        return this.id;
    }

    public String getContent() {
        return this.content;
    }
}
